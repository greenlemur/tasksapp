<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlFieldToActionsTable extends Migration
{
    /**
     * Update Actions table to include URL field
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function ($table) {
            $table->string('url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actions', function ($table) {
            $table->dropColumn('url');
        });
    }
}
