<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectOrderFieldToActions extends Migration
{
    /**
     * Update Actions table to include field for ordering
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function ($table) {
            $table->integer('project_order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actions', function ($table) {
            $table->dropColumn('project_order');
        });
    }
}
