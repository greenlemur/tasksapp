<?php

namespace Database\Seeds;

use App\Action;
use App\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgendaTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actions')->truncate();
        DB::table('users')->truncate();
        DB::table('clients')->truncate();

        Client::factory(2)->create();

        Action::factory()->create([
            'title' => 'Action not flagged',
            'client_id' => 1,
            'project_order' => 1
        ]);
        Action::factory()->create([
            'flagged' => 1,
            'title' => 'First flagged action',
            'client_id' => 1,
            'project_order' => 2
        ]);
        Action::factory()->create([
            'flagged' => 1,
            'title' => 'Second flagged action',
            'client_id' => 2,
            'project_order' => 3
        ]);
    }
}
