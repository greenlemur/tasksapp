<?php

namespace Database\Seeds;

use App\Action;
use App\Client;
use App\Project;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->truncate();
        DB::table('projects')->truncate();

        Client::factory()->create();
        Project::factory()->create([ 'client_id' => 1, 'title' => 'Test project 1' ]);
        Project::factory()->create([ 'title' => 'Test project 2' ]);
        Project::factory()->create([ 'title' => 'Test project 3' ]);

        Action::factory()->create([ 'client_id' => 1, 'project_id' => 1, 'title' => 'Test action 1' ]);
        Action::factory()->create([ 'title' => 'Test action 2' ]);
        Action::factory()->create([ 'title' => 'Test action 3' ]);
    }
}
