@extends('templates/main')

@section('content')
<h1>Journal of completed actions</h1>
<div class="project-container">
    <div class="project-row">
        <span class="project-title project-toggle">Today</span>
    </div>
    <div class="action-container">
        @foreach ($deletedtoday as $action)
            @include('partials.show_action')
        @endforeach
    </div>
</div>
<div class="project-container">
    <div class="project-row">
        <span class="project-title project-toggle">Yesterday</span>
    </div>
    <div class="action-container">
        @foreach ($deletedyesterday as $action)
            @include('partials.show_action')
        @endforeach
    </div>
</div>
<div class="project-container">
    <div class="project-row">
        <span class="project-title project-toggle">Last week</span>
    </div>
    <div class="action-container">
        @foreach ($deletedlastweek as $action)
            @include('partials.show_action')
        @endforeach
    </div>
</div>
@endsection
