{!! Form::open(['route' => 'client.store', 'id' => 'client-detail-form']) !!}

<a href="#" class="close-dynamic-form visible-link">close</a>
<h3>Create new client</h3>

<!-- Client title -->
<div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['id' => 'client-form-title', 'class' => 'form-control', 'autofocus']) !!}
</div>

<!-- Client notes -->
<div class="form-group">
    {!! Form::label('notes', 'Notes') !!}
    {!! Form::textarea('notes', null, ['id' => 'client-form-notes', 'class' => 'form-control']) !!}
</div>

<!-- Add Client Button -->
<div class="form-group">
    {!! Form::label('submit_button',' ') !!}
    {!! Form::submit('Store client', ['class' => 'form-button', 'id' => 'client-form-button']) !!}
</div>

<div class="form-group">
    <label for="addAnotherClient">Add another</label>
    <input type="checkbox" id="client-form-addanother" name="addAnotherClient" class="create-checkbox add-another">
</div>


{!! Form::close() !!}
