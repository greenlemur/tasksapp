{!! Form::open(['route' => 'action.store', 'id' => 'action-form']) !!}

<a href="#" class="close-dynamic-form visible-link">close</a>
<h3>Create new action</h3>

<!-- Project title -->
<div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['id' => 'action-form-title', 'class' => 'form-control', 'autofocus']) !!}
</div>

<!-- Project date due -->
<div class="form-group">
    {!! Form::label('due', 'Date due') !!}
    {!! Form::text('due', null, ['id' => 'action-form-due', 'class' => 'form-control dateinput']) !!}
</div>

<!-- Project notes -->
<div class="form-group">
    {!! Form::label('notes', 'Notes') !!}
    {!! Form::textarea('notes', null, ['id' => 'action-form-notes', 'class' => 'form-control', 'rows' => 8]) !!}
</div>

<!-- Project URL -->
<div class="form-group">
    {!! Form::label('url', 'URL') !!}
    {!! Form::text('url', null, ['id' => 'action-form-url', 'class' => 'form-control']) !!}
</div>

<!-- Related client -->
<div class="form-group">
{!! Form::label('client_id','Client') !!}
    <select class="form-control" id="action-form-client_id" name="client_id">
        <option value=0 @if(0 == $client_id) selected @endif>
            -- no client --
        </option>
        @foreach($active_clients as $client)
            <option value="{{ $client->id }}" @if($client->id == $client_id) selected @endif>
                {{ $client->title }}
            </option>
        @endforeach
    </select>
</div>

<!-- Related project -->
<div class="form-group">
{!! Form::label('project_id','Project') !!}
<!--suppress HtmlFormInputWithoutLabel -->
    <select class="form-control" id="action-form-project_id" name="project_id">
        <option value=0 @if(0 == $project_id) selected @endif>
            -- no project --
        </option>
        @foreach($active_projects as $project)
            @if ($project->client_id == $client_id)
                <option value="{{ $project->id }}" @if($project->id == $project_id) selected @endif>
                    {{ $project->title }}
                </option>
            @endif
        @endforeach
    </select>
</div>

<!-- Project flagged status -->
<div class="form-group">
    {!! Form::label('flagged', 'Flag?', ['class' => 'label-flagged']) !!}
    {!! Form::checkbox('flagged', false) !!}
</div>

<!-- Add Client Button -->
<div class="form-group">
    {!! Form::label('submit_button',' ') !!}
    {!! Form::submit('Store action', ['class' => 'form-button', 'id' => 'action-form-button']) !!}
</div>

<div class="form-group">
    <label for="addAnotherAction">Add another</label>
    <input type="checkbox" id="action-form-addanother" name="addAnotherAction" class="create-checkbox add-another">
</div>

{!! Form::close() !!}
