{!! Form::open(['route' => 'note.store', 'method' => 'POST', 'id' => 'note-form']) !!}

<a href="#" class="close-dynamic-form visible-link">close</a>
<h3>Create a note</h3>

<!-- Note id -->
{{ Form::hidden('id', 0, ['id' => 'note-form-id']) }}

<!-- Note title -->
<div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['id' => 'note-form-title', 'class' => 'form-control', 'placeholder' => 'add a title', 'autofocus', 'required']) !!}
</div>

<!-- Related client -->
<div class="form-group">
    {!! Form::label('client_id', 'Client') !!}
    <select class="form-control" id="note-form-client_id" name="client_id">
        <option value=0 @if(0 == $client_id) selected @endif>
            -- no client --
        </option>
        @foreach($active_clients as $client)
            <option value="{{ $client->id }}" @if($client->id == $client_id) selected @endif>
                {{ $client->title }}
            </option>
        @endforeach
    </select>
</div>

<!-- Note tags -->
<div class="form-group">
    {!! Form::label('tags', 'Tags') !!}
    {!! Form::text('tags', null, [ 'id' => 'note-form-tags', 'class' => 'form-control']) !!}
</div>

<!-- Note body -->
<div class="form-group">
    {!! Form::label('body', 'Body') !!}
    {!! Form::textarea('body', null, ['id' => 'note-form-body', 'class' => 'form-control', 'placeholder' => 'write your note in here', 'rows' => 10]) !!}
</div>

<!-- Add Save and leave Button -->
<div class="form-group">
    {!! Form::label('submit_button',' ') !!}
    {!! Form::submit('Save and close', ['class' => 'form-button note-form-button', 'id' => 'note-save-and-leave']) !!}
</div>
<!-- Add Save and keep editing Button -->
<div class="form-group">
    {!! Form::label('submit_button',' ') !!}
    {!! Form::submit('Save and keep editing', ['class' => 'form-button note-form-button', 'id' => 'note-save-and-stay']) !!}
</div>

{!! Form::close() !!}
