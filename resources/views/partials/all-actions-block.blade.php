<div id="all-actions-block">
    <div id="aab-container">
        <div class="close-link">
            <a href="#" class="visible-link">close</a>
        </div>
        <h2>Keyboard shortcuts</h2>

        <div class="aab aab-col-1">
            <h3>View...</h3>
            <ul>
                <li><strong>v</strong>iew <strong>a</strong>ll (<strong>va</strong>)</li>
                <li><strong>v</strong>iew <strong>m</strong>y day (<strong>vm</strong>)</li>
                <li><strong>v</strong>iew <strong>j</strong>ournal (completed) (<strong>vj</strong>)</li>
                <li><strong>v</strong>iew <strong>n</strong>otes (<strong>vn</strong>)</li>
            </ul>
        </div>
        <div class="aab aab-col-2">
            <h3>Create...</h3>
            <ul>
                <li><strong>c</strong>reate <strong>c</strong>lient (<strong>c c</strong>)</li>
                <li><strong>c</strong>reate <strong>p</strong>roject (<strong>c p</strong>)</li>
                <li><strong>c</strong>reate <strong>a</strong>ction (<strong>c a</strong>)</li>
            </ul>
        </div>
        <div class="aab aab-col-3">
            <h3>User...</h3>
            <ul>
                <li><strong>u</strong>ser <strong>a</strong>dmin (<strong>u a</strong>)</li>
                <li><strong>u</strong>ser <strong>h</strong>elp (<strong>u h</strong>)</li>
                <li><strong>u</strong>ser <strong>l</strong>ogout (<strong>u l</strong>)</li>
            </ul>
        </div>
        <p class="aab-footer">Get more in-depth help on the <a href="/help" class="visible-link">main help page</a>.</p>
    </div>
</div>