@if(0 == $client_id)
    <option value=0 selected>-- show all --</option>
@else
    <option value=0>-- show all --</option>
@endif

@foreach ($clients as $client)
    @if($client->id == $client_id)
        <option value="{{ $client->id }}" selected>{{ $client->title }}</option>
    @else
        <option value="{{ $client->id }}">{{ $client->title }}</option>
    @endif
@endforeach
