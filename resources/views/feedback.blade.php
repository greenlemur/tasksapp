@extends('templates/main')

@section('content')
<h1>Let me know what you think</h1>
    <p>tasksapp was built to fulfil a need for me (you can read more about this
        over on the <a href="/help">help page</a>), but I want to
        make it as useful as I can for all users so I'd really appreciate any
        feedback:</p>
    <ul>
        <li>what do you like?</li>
        <li>what doesn't work as you think it should?</li>
        <li>what's missing?</li>
    </ul>
    <p>Enter your feedback below and then hit the submit button to email me with
        your thoughts. I'll try and respond to any questions as quickly as I
        can, and all suggestions for improvements or fixes will be added to my
        'tasksapp feature requests' project.</p>

        <form action="/feedback" method="post" class="feedback-form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="message">Feedback</label>
                    <textarea type="text" id="message" name="message" rows="10" cols="50"></textarea>
            </div>
            <div class="form-group">
                <label for="submit-button"></label>
                    <input type="submit" id="submit-button" name="submit-button" value="Submit feedback"></input>
            </div>
        </form>

@endsection
