@extends('templates/main')

@section('content')
    @if(null == $client_id)
        <div class="client-container clientSortable" client_id="0">
            <div class="client-row">
                <span class="client-title is-open">Inbox</span>
                <span class="client-icons">
                    <a href="{{ route('action.create') }}" title="Add an action to your inbox" id="icon-add-action-to-inbox">
                        <svg viewBox="0 0 8 8" class="icon">
                            <use xlink:href="/images/open-iconic.svg#plus"></use>
                        </svg>
                    </a>
                </span>
            </div>
            <div class="action-container action-container-client-{{ $client_id or 0 }}" id="inbox-container">
                @foreach ($orphan_actions as $action)
                    @include('partials.show_action')
                @endforeach
            </div>
            @foreach ($orphan_projects as $project)
                @include('partials.show_project')
            @endforeach
        </div><!-- .client-container -->
    @endif
    @if($clients)
        @foreach ($clients as $client)
            @include('partials.show_client')
        @endforeach
    @endif

@endsection
