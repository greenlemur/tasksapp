@extends('templates/main')

@section('content')
<div class="client-container">
    <div class="client-row">
        <span class="client-title is-open">Inbox</span>
        <span class="client-icons">
            <a href="{{ route('action.create') }}" title="Add an action to your inbox" id="icon-add-action-to-inbox">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#plus"></use>
                </svg>
            </a>
        </span>
    </div>
    <div class="action-container action-container-client-0" id="inbox-container">
        @foreach ($inbox_actions as $action)
            @include('partials.agendaAndSearchView')
        @endforeach
    </div>
    @foreach ($inbox_projects as $project)
        @include('partials.show_project')
    @endforeach
</div><!-- .client-container -->

<div class="client-container">
    <div class="client-row">
        <span class="client-title">My day</span>
    </div>

    <div class="project-container" type="due-today">
        <div class="project-row">
            <span class="project-title">Tasks due today</span>
        </div>
        <div class="action-container due-actions">
            @foreach ($duetoday as $action)
                @include('partials.agendaAndSearchView')
            @endforeach
        </div>
    </div>

    <div class="project-container" type="flagged">
        <div class="project-row">
            <span class="project-title">Flagged tasks</span>
        </div>
        <div class="action-container flagged-actions">
            @foreach ($flagged as $action)
                @include('partials.agendaAndSearchView')
            @endforeach
        </div>
    </div>

    <div class="project-container" type="due-next-week">
        <div class="project-row">
            <span class="project-title">Tasks due in the future</span>
        </div>
        <div class="action-container future-actions">
            @foreach ($duefuture as $action)
                @include('partials.agendaAndSearchView')
            @endforeach
        </div>
    </div>
</div>
@endsection
