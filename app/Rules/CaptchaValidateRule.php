<?php

namespace App\Rules;

use App\Interfaces\CaptchaValidationManager;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;

class CaptchaValidateRule implements Rule
{
    private CaptchaValidationManager $captchaManager;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->captchaManager = App::make(CaptchaValidationManager::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return $this->captchaManager->validate_captcha($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'CAPTCHA could not be validated - please try again';
    }
}
