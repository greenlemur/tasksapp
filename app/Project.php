<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Request;

class Project extends Model
{
    use SoftDeletes, HasFactory;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $fillable = [
        'title',
        'notes',
        'client_id',
        'client_order',
        'user_id',
        'created_at'
    ];
    protected $casts = [
        'client_id' => 'integer',
        'user_id' => 'integer',
        'client_order' => 'integer',
    ];


    // Helper methods for moving projects around
    /**
     * Get the correct client ID for the current project
     *
     * @return int
     */
    public function getClientId()
    {
        if ($this->client) {
            $client_id = $this->client->id;
        } elseif (null != session('client_id')) {
            $client_id = session('client_id');
        } else {
            $client_id = 0;
        }
        return $client_id;
    }

    /**
     * Update project and siblings if it is moving in the project structure
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function moveProject(\Illuminate\Http\Request $request) : void
    {
        $new_params = [
            'client_id' => intval(Request::input('client_id', $this->client_id)),
            'client_order' => intval(Request::input('client_order', $this->client_order)),
        ];
        if ($this->shouldMoveProject($new_params)) {
            $this->removeFromOrder();
            $this->addToOrder($request->input('client_id'), $request->input('client_order'));

            // unset the values that were amended above to prevent propagation
            unset($request['client_id']);
            unset($request['client_order']);
        }
    }

    /**
     * Determine if the data passed int he request means the project is moving or just updating
     *
     * @param array $params
     * @return bool
     */
    public function shouldMoveProject(array $params) : bool
    {
        return $params['client_id'] !== $this->client_id
            || $params['client_order'] !== $this->client_order;
    }
    /**
     * Remove project from client
     *
     * Delete the client_order for the current project and re-order
     * remaining siblings
     *
     * @return bool
     */
    public function removeFromOrder()
    {
        foreach ($this->getSiblings() as $project) {
            if ($project->client_order > $this->client_order) {
                $project->client_order--;
                $project->save();
            }
        }

        $this->client_order = 0;
        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Insert project into new Client with correct ordering
     *
     * Set client_order for this project and re-order new siblings to
     * preserve correct overall order
     *
     * @param $client_id
     * @param $client_order
     * @return bool
     */
    public function addToOrder($client_id, $client_order)
    {
        $this->client_id = $client_id;
        $this->save();

        foreach ($this->getSiblings() as $project) {
            if ($project !== $this && $project->client_order >= $client_order) {
                $project->client_order++;
                $project->save();
            }
        }
        foreach ($this->actions as $action) {
            $action->client_id = $client_id;
            $action->save();
        }

        $this->client_order = $client_order;

        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Get list of other projects in client
     *
     * @return Collection
     */
    protected function getSiblings()
    {
        if ($this->client_id > 0) {
            $projects = Project::where('client_id', $this->client_id)->get();
        } else {
            $projects = $this->user->inboxProjects();
        }

        return $projects;
    }



    // Other public methods
    /**
     * Check if the active project has any actions still live
     *
     * @return bool
     */
    public function canBeDeleted()
    {
        if ($this->actions()->count()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Find the current highest client_order
     *
     * @return int
     */
    public function calculateClientOrder() : int
    {
        $max = DB::table('projects')
            ->where('client_id', $this->client_id)
            ->where('user_id', auth()->user()->id)
            ->pluck('client_order')
            ->max();

        return $max > 0 ? $max + 1 : 1;
    }



    // Query scopes
    /**
     * Create local scope to get projects over a week old
     *
     * @param $query
     * @return mixed
     */
    public function scopeOneWeekOld($query)
    {
        $aWeekAgo = Carbon::today()->subDays(7);
        return $query->where('created_at', '>', $aWeekAgo);
    }



    // Relationships
    /**
     * Get the user that the project belongs to
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the client that the project belongs to
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get all of the actions for the project.
     */
    public function actions()
    {
        return $this->hasMany(Action::class);
    }

    /**
     * Get all of the notes for the client.
     */
    public function notes()
    {
        return $this->hasMany(Note::class);
    }
}
