<?php

namespace App\Observers;

use App\Classes\TokenManager;
use App\Mail\NewUserRegisters;
use App\User;
use Illuminate\Support\Facades\Mail;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  User  $user
     * @return void
     */
    public function creating(User $user)
    {
    }
}
