<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RegistrationToken extends Model
{
    protected $table = 'registration_tokens';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'lookup';

    protected $dates = [
        'expiry',
    ];
    protected $fillable = [
        'email',
        'expiry',
        'lookup',
        'token',
    ];
    protected $casts = [ ];

    /**
     * Validate the user registration token
     *
     * @param string $token
     * @return bool
     */
    public function is_valid(string $token): bool {
        return password_verify($token, $this->token)
            && $this->expiry > Carbon::now()->subDays(2);
    }
}
