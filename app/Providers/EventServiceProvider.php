<?php

namespace App\Providers;

use App\Client;
use App\Project;
use App\User;
use App\Note;
use App\Action;
use App\Observers\{ActionObserver, ClientObserver, NoteObserver, ProjectObserver, UserObserver};
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Note::observe(NoteObserver::class);
        User::observe(UserObserver::class);
        Client::observe(ClientObserver::class);
        Action::observe(ActionObserver::class);
        Project::observe(ProjectObserver::class);
    }
}
