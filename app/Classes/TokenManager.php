<?php

namespace App\Classes;

use App\Exceptions\TokenNotValidException;
use App\RegistrationToken;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Boolean;

class TokenManager {
    /**
     * @param User $user
     * @return array
     */
    public function create_token(User $user): array
    {
        $plain_token = Str::random(64);

        $token = RegistrationToken::create([
            'email' => $user->email,
            'expiry' => Carbon::now()->addHours(48),
            'lookup' => Str::random(24),
            'plain_token' => $plain_token,
            'token' => password_hash($plain_token, PASSWORD_BCRYPT),
        ]);

        return [
            'lookup' => $token->lookup,
            'token' => $plain_token,
        ];
    }

    /**
     * @param string $lookup
     * @param string $token
     * @return RegistrationToken
     * @throws TokenNotValidException
     */
    public function validate_token_from_lookup(string $lookup, string $token): RegistrationToken
    {
        $stored_token = RegistrationToken::where('lookup', $lookup)->first();

        if ($stored_token == null || ! $stored_token->is_valid($token)) {
            throw new TokenNotValidException();
        }

        return $stored_token;
    }
}
