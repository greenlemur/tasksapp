<?php

namespace App\Http\Controllers\Action;

use App\Action;
use App\Http\Requests\UpdateAction;
use App\Project;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\StoreAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ActionController extends Controller
{
    /**
     * Return the actionrow for the given action
     *
     * @param  Request $request
     * @param  Action  $action
     * @return Response
     */
    public function show(Request $request, Action $action): Response
    {
        return response($action->buildActionRow($request), 200);
    }

    /**
     * Show the form to create a new action
     *
     * Captures the URL the user came from so we can direct them back to where
     * they were once the action is created
     *
     * @param Request $request
     * @return View|Response
     */
    public function create(Request $request, $project_id = 0)
    {
        if ($project_id > 0) {
            $client_id = Project::find($project_id)->client_id;
        } else {
            $client_id = session()->has('client_id') ? session('client_id') : 0;
        }

        if ($request->isJson() || $request->ajax()) {
            return response(view('forms.action_create_form')
                ->with('client_id', $client_id)
                ->with('project_id', $project_id), 200);
        } else {
            return view('action.create')
                ->with('client_id', $client_id)
                ->with('project_id', $project_id);
        }
    }

    /**
     * Take form data and use it to create a new action
     *
     * @param StoreAction $request
     *
     * @return RedirectResponse|Response
     */
    public function store(StoreAction $request)
    {
        $action = Action::create($request->all());

        if ($request->isJson() || $request->ajax()) {
                return response($action, 201);
        } else {
            if ($request['addAnotherAction'] == "on") {
                return redirect()->route('action.create');
            }
            return redirect('home');
        }
    }

    /**
     * Show the form to edit an existing action
     *
     * @param Request $request
     * @param Action $action
     * @return Response|View
     */
    public function edit(Request $request, Action $action)
    {
        $action->calculateProjectOrder();
        if ($request->isJson() || $request->ajax()) {
            return response(view('forms.action_update_form')
                ->with('action', $action)
                ->with('client_id', $action->client_id)
                ->with('project_id', $action->project_id), 200);
        } else {
            return view('action.edit')
                ->with('action', $action)
                ->with('client_id', $action->client_id)
                ->with('project_id', $action->project_id);
        }
    }

    /**
     * Take form data and use it to update an existing action
     *
     * @param Action $action
     * @param UpdateAction $request
     *
     * @return RedirectResponse|Response
     */
    public function update(Action $action, UpdateAction $request)
    {
        $action->moveAction($request);
        $action->update($request->all());

        return ($request->isJson() || $request->ajax())
            ? response($action, 201)
            : redirect('home');
    }
}
