<?php
namespace App\Http\Controllers\Action;

use App\Action;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TitleController extends Controller
{
    /**
     * Return the dynamic title input field for display
     *
     * @param Action $action
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function edit(Action $action)
    {
        return response(
            view('action.partials.dynamic_title_form')
                ->with('action', $action)
                ->render(),
            200
        );
    }

    /**
     * Update the action title and return the updated HTML
     *
     * @param Request $request
     * @param Action $action
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function update(Request $request, Action $action)
    {
        $action->title = $request->input('title');
        $action->save();

        return response($action->buildActionRow($request), 200);
    }
}
