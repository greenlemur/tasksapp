<?php

namespace App\Http\Controllers\View;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AgendaController extends Controller
{
    /**
     * Shows an agenda-style view
     *
     * Stem of query differs depending on whether we know the client required
     * or not. Note the use of clone in this method as the query gets mutated
     * by previous calls otherwise, leading to unexpected data.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $client_id = session('client_id', null);
        $flagged = auth()->user()->actions()->flagged()->notDue()->whereClientIs($client_id)
            ->orderBy('project_order', 'asc')
            ->get()->load('project');

        return view('agenda')
            ->with('inbox_actions', auth()->user()->inboxActions()->get()->load('project'))
            ->with('inbox_projects', auth()->user()->inboxProjects()->get()->load('client'))
            ->with('duetoday', auth()->user()->actions()->dueToday()->get()->load('project'))
            ->with('duefuture', auth()->user()->actions()->dueInFuture()->get()->load('project'))
            ->with('flagged', $flagged);
    }
}
