<?php

namespace App\Http\Controllers\View;

use App\User;
use App\Feedback;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;

class SuperAdminController extends Controller
{
    /**
     * Show all user stats to the super user
     *
     * Database flag marks the user as su or not, and the view only renders
     * if the user has this applied to them
     *
     * @return View
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('view-all-users', auth()->user());
        $users = User::with(['clients', 'projects', 'actions'])
            ->orderBy('lastLoggedInOn', 'desc')
            ->get();

        return view('admin.super')
            ->with('users', $users)
            ->with('feedback', Feedback::with('user')->get());
    }
}
