<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;

class SwitchController extends Controller
{
    /**
     * Switch the active client in the session
     *
     * @param int $client_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index($client_id)
    {
        if (0 == $client_id) {
            session(['client_id' => null]);
        } else {
            session(['client_id' => $client_id]);
        }
        return redirect()->back();
    }
}
