<?php
namespace App\Http\Controllers\Project;

use App\Project;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProject;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends Controller
{
    /**
     * Return the projectrow for the given project
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $data = view('partials.show_project')->with('project', $project)->with('client_id', $project->client_id);
        if ($data) {
            return response($data, 200);
        } else {
            return response('Couldn\'t fetch project data!', 200);
        }
    }
    /**
     * Show the form to allow user to create a project
     *
     * @param Request $request
     * @param int $client_id
     * @return $this
     */
    public function create(Request $request, $client_id = 0)
    {
        if ((0 == $client_id) && (null != session('client_id'))) {
            $client_id = session('client_id');
        }

        if ($request->isJson() || $request->ajax()) {
            return response(
                view('forms.project_create_form')->with('client_id', $client_id),
                200
            );
        } else {
            return view('project.create')->with('client_id', $client_id);
        }
    }

    /**
     * Take form data and use it to create a new project
     *
     * @param StoreProject $request
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function store(StoreProject $request)
    {
        $project = Project::create($request->all());

        if ($request->isJson() || $request->ajax()) {
            return response([
                'html' => view('partials.show_project')->with('project', $project)->render(),
                'client_id' => $project->getClientId()
            ], 201);
        } else {
            $route = $request['addAnotherProject'] == "on" ? 'project.create' : 'home';
            return redirect()->route($route);
        }
    }

    /**
     * Show form to edit an existing project
     *
     * @param Request $request
     * @param Project $project
     * @return $this
     */
    public function edit(Request $request, Project $project)
    {
        if ($request->isJson() || $request->ajax()) {
            return response(
                view('forms.project_update_form')->with('project', $project)
                    ->with('client_id', $project->client_id),
                200
            );
        } else {
            return view('project.edit')->with('project', $project)
                ->with('client_id', $project->client_id);
        }
    }

    /**
     * Take form data and use it to update an existing project
     *
     * @param Request $request
     * @param Project $project
     * @return RedirectResponse|Response
     */
    public function update(Request $request, Project $project)
    {
        $project->moveProject($request);

        $project->update($request->all());

        return ($request->isJson() || $request->ajax())
            ? response($project, 200)
            : redirect()->back();
    }

    /**
     * Mark a project as completed
     *
     * Only runs if the project does not contain any live actions
     *
     * @param Project $project
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request, Project $project)
    {
        if ($project->canBeDeleted()) {
            $project->removeFromOrder();
            $project->delete();

            if ($request->isJson() || $request->ajax()) {
                return response('success', 200);
            } else {
                return redirect()->route('client.show', ['client'=>$project->client_id]);
            }
        } else {
            if ($request->isJson() || $request->ajax()) {
                return response("Sorry, you can't delete a project if it has active actions", 400);
            } else {
                return redirect()
                    ->route('client.index', ['client'=>$project->client_id])
                    ->with('status', "Can't delete while the project has active actions!");
            }
        }
    }
}
