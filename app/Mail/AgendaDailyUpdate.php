<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Carbon\Carbon;

class AgendaDailyUpdate extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $flagged;
    public $due;

    /**
     * AgendaDailyUpdate constructor.
     * @param User $user
     * @param Collection $flagged
     * @param Collection $due
     */
    public function __construct($user, $flagged, $due)
    {
        $this->user = $user;
        $this->flagged = $flagged->count() > 0 ? $flagged->get() : [];
        $this->due = $due->count() > 0 ? $due->get() : [];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->user->getFirstName() . ', these are your urgent tasks')
                    ->view('emails.dailyagenda');
    }
}
