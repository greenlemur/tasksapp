<?php

namespace Tests\Unit;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjectTest extends TestCase
{
    use DatabaseMigrations;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function can_get_correct_client_id_for_any_project()
    {
        $user = User::factory()->create();
        $this->be($user);

        $project1 = Project::factory()->create([
            'user_id' => $user->id,
        ]);
        $client = Client::factory()->create();
        $project2 = Project::factory()->create([
            'user_id' => $user->id,
            'client_id' => $client->id
        ]);

        $this->assertEquals(0, $project1->getClientId());
        $this->assertEquals(1, $project2->getClientId());
    }
    /** @test */
    public function can_get_correct_client_id_from_the_session()
    {
        $user = User::factory()->create();
        $this->be($user);

        session([
            'client_id' => 999
        ]);
        $project = Project::factory()->create([
            'user_id' => $user->id,
        ]);

        $this->assertEquals(999, $project->getClientId());
    }
    /** @test */
    public function correct_client_order_calculated_for_first_project()
    {
        $project = Project::factory()->create();

        $this->assertEquals(1, $project->calculateClientOrder());
    }
    /** @test */
    public function correct_client_order_calculated_for_single_project()
    {
        $project = Project::factory()->create(['client_order' => 5]);

        $this->assertEquals(6, $project->calculateClientOrder());
    }
    /** @test */
    public function project_with_no_actions_can_be_deleted()
    {
        // Arrange
        Client::factory()->create();
        $project = Project::factory()->create(['client_id' => 1]);

        // Assert
        $this->assertTrue($project->canBeDeleted());
    }
    /** @test */
    public function project_with_live_actions_cannot_be_deleted()
    {
        // Arrange
        Client::factory()->create();
        $project = Project::factory()->create(['client_id' => 1]);
        Action::factory(3)->create(['client_id' => 1, 'project_id' => 1]);

        // Assert
        $this->assertFalse($project->canBeDeleted());
    }
    /** @test */
    public function project_with_completed_actions_can_be_deleted()
    {
        // Arrange
        Client::factory()->create();
        $project = Project::factory()->create(['client_id' => 1]);
        $actions = Action::factory(3)
                     ->create([ 'client_id' => 1, 'project_id' => 1 ]);

        // Act
        foreach ($actions as $action) {
            $action->deleted_at = Carbon::now();
            $action->save();
        }

        // Assert
        $this->assertTrue($project->canBeDeleted());
    }
}
