<?php

namespace Tests\Unit;

use App\Action;
use App\Project;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DebugViewTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function user_can_set_debug_view()
    {
        $this->assertNull($this->user->debug_view);

        $this->user->update([
            'debug_view' => 1,
        ]);

        $this->assertEquals(1, $this->user->fresh()->debug_view);
    }

    /** @test */
    public function user_sees_extra_information_for_actions()
    {
        $this->user->update([
            'debug_view' => 1,
        ]);

        $action = Action::factory()->create([
            'title' => 'Testing the action.show action',
            'notes' => 'There are copious notes for this action',
            'user_id' => 1,
            'project_order' => 3,
        ]);

        $actionView = view('partials.show_action')
            ->with('action', $action)
            ->render();

        $this->assertStringContainsString(
            'Testing the action.show action [3]',
            $actionView,
        );
    }

    /** @test */
    public function user_sees_extra_information_for_projects()
    {
        $this->user->update([
            'debug_view' => 1,
        ]);

        $project = Project::factory()->create([
            'title' => 'Testing the project.show action',
            'notes' => 'There are copious notes for this action',
            'user_id' => 1,
            'client_order' => 42,
        ]);

        $projectView = view('partials.show_project')
            ->with('project', $project)
            ->render();

        $this->assertStringContainsString(
            'Testing the project.show action [42]',
            $projectView,
        );
    }
}
