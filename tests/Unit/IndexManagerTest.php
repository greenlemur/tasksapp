<?php

namespace Tests\Unit;

use App\Action;
use App\Classes\IndexManager;
use App\Client;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Database\Seeds\IndexManagerTestSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class IndexManagerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);

        $this->artisan('db:seed', [ '--class' => IndexManagerTestSeeder::class ]);
    }

    /** @test */
    public function can_get_incorrect_order_before_sorting()
    {
        $clients = Client::with('projects.actions')->get();
        $inbox = Action::where([
            'client_id' => 0,
            'project_id' => 0,
        ])->get();

        $this->assertEquals(42, $inbox[0]->project_order);
        $this->assertEquals(44, $inbox[1]->project_order);
        $this->assertEquals(57, $inbox[2]->project_order);

        $this->assertEquals(2, $clients[0]->actions[0]->project_order);
        $this->assertEquals(9, $clients[0]->actions[1]->project_order);
        $this->assertEquals(101, $clients[0]->actions[2]->project_order);

        $this->assertEquals(5, $clients[0]->projects[0]->client_order);
        $this->assertEquals(0, $clients[0]->projects[0]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->projects[0]->actions[1]->project_order);
        $this->assertEquals(10, $clients[0]->projects[0]->actions[2]->project_order);

        $this->assertEquals(9, $clients[0]->projects[1]->client_order);
        $this->assertEquals(3, $clients[0]->projects[1]->actions[0]->project_order);
        $this->assertEquals(3, $clients[0]->projects[1]->actions[1]->project_order);

        $this->assertEquals(72, $clients[1]->actions[0]->project_order);
        $this->assertEquals(73, $clients[1]->actions[1]->project_order);
        $this->assertEquals(74, $clients[1]->actions[2]->project_order);
        $this->assertEquals(75, $clients[1]->actions[3]->project_order);
    }

    /** @test */
    public function can_get_correct_project_order_after_sorting()
    {
        $im = new IndexManager();
        $im->indexProjects();

        $clients = Client::with('projects.actions')->get();
        $inbox = Action::where([
            'client_id' => 0,
            'project_id' => 0,
        ])->get();

        $this->assertEquals(1, $clients[0]->projects[0]->client_order);
        $this->assertEquals(2, $clients[0]->projects[1]->client_order);


        // Actions should not be affected by the action index method
        $this->assertEquals(42, $inbox[0]->project_order);
        $this->assertEquals(44, $inbox[1]->project_order);
        $this->assertEquals(57, $inbox[2]->project_order);

        $this->assertEquals(2, $clients[0]->actions[0]->project_order);
        $this->assertEquals(9, $clients[0]->actions[1]->project_order);
        $this->assertEquals(101, $clients[0]->actions[2]->project_order);

        $this->assertEquals(0, $clients[0]->projects[0]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->projects[0]->actions[1]->project_order);
        $this->assertEquals(10, $clients[0]->projects[0]->actions[2]->project_order);

        $this->assertEquals(3, $clients[0]->projects[1]->actions[0]->project_order);
        $this->assertEquals(3, $clients[0]->projects[1]->actions[1]->project_order);

        $this->assertEquals(72, $clients[1]->actions[0]->project_order);
        $this->assertEquals(73, $clients[1]->actions[1]->project_order);
        $this->assertEquals(74, $clients[1]->actions[2]->project_order);
        $this->assertEquals(75, $clients[1]->actions[3]->project_order);
    }

    /** @test */
    public function can_get_correct_action_order_after_sorting()
    {
        $im = new IndexManager();
        $im->indexActions();

        $clients = Client::with('projects.actions')->get();
        $inbox = Action::where([
            'client_id' => 0,
            'project_id' => 0,
        ])->get();

        $this->assertEquals(1, $inbox[0]->project_order);
        $this->assertEquals(2, $inbox[1]->project_order);
        $this->assertEquals(3, $inbox[2]->project_order);

        $this->assertEquals(1, $clients[0]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->actions[1]->project_order);
        $this->assertEquals(3, $clients[0]->actions[2]->project_order);

        $this->assertEquals(1, $clients[0]->projects[0]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->projects[0]->actions[1]->project_order);
        $this->assertEquals(3, $clients[0]->projects[0]->actions[2]->project_order);

        $this->assertEquals(1, $clients[0]->projects[1]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->projects[1]->actions[1]->project_order);

        $this->assertEquals(1, $clients[1]->actions[0]->project_order);
        $this->assertEquals(2, $clients[1]->actions[1]->project_order);
        $this->assertEquals(3, $clients[1]->actions[2]->project_order);
        $this->assertEquals(4, $clients[1]->actions[3]->project_order);

        // Projects should not be affected by the action index method
        $this->assertEquals(5, $clients[0]->projects[0]->client_order);
        $this->assertEquals(9, $clients[0]->projects[1]->client_order);
    }
}
