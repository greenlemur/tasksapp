<?php

namespace Tests\Feature\Client;

use App\User;
use App\Client;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SwitchTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function client_is_updated_via_ajax_request()
    {
        $client1 = Client::factory()->create();
        $client2 = Client::factory()->create();

        $this->json('GET', '/client/switch/0');
        $this->assertNull(session('client_id'));

        $this->json('GET', '/client/switch/' . $client1->id);
        $this->assertEquals($client1->id, session('client_id'));

        $this->json('GET', '/client/switch/' . $client2->id);
        $this->assertEquals($client2->id, session('client_id'));
    }
}
