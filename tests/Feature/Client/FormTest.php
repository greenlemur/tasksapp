<?php

namespace Tests\Feature\Client;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FormTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function create_client_form_shows_correctly()
    {
        $ajax_response = $this->json('GET', '/client/create');
        $static_response = $this->call('GET', '/client/create');

        $ajax_response->assertSee('<h3>Create new client</h3>', false);
        $static_response->assertSee('<h3>Create new client</h3>', false);
    }
    /** @test */
    public function update_client_form_shows_correctly()
    {
        $client = Client::factory()->create([ 'title' => 'Fake client' ]);

        $ajax_response = $this->json('GET', '/client/' . $client->id . '/edit');
        $static_response = $this->call('GET', '/client/' . $client->id . '/edit');

        $ajax_response
            ->assertSee('<h3>Update client</h3>', false)
            ->assertSee('Fake client', false);
        $static_response
            ->assertSee('<h3>Update client</h3>', false)
            ->assertSee('Fake client', false);
    }
}
