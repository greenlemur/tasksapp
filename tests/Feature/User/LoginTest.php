<?php

namespace Tests\Feature\User;

use App\Mail\NewUserRegisters;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function setUp():void
    {
        parent::setUp();
    }

    /** @test */
    public function user_gets_logged_in_from_remote_login_link()
    {
        $user = User::factory()->create();
        $response = $this->call('GET', '/remotelogin?daily=' . $user->api_token);

        $response->assertRedirect('/');
        $this->assertEquals($user->id, auth()->user()->id);
    }
}
