<?php

namespace Tests\Feature\User;

use App\Classes\TokenManager;
use App\RegistrationToken;
use App\User;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ValidateTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;
    protected array $token_params;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $tm = new TokenManager();
        $this->token_params = $tm->create_token($this->user);
    }

    /** @test */
    public function validate_link_validates_user()
    {
        $this->assertNull($this->user->validated_at);

        $url = '/validate-user?l='
            . $this->token_params['lookup']
            . '&t='
            . $this->token_params['token'];

        $this->call('Get', $url);

        $this->assertNotNull($this->user->fresh()->validated_at);

        $this->assertEquals(0, RegistrationToken::count());
    }

    /** @test */
    public function validated_user_is_logged_in()
    {
        $url = '/validate-user?l='
            . $this->token_params['lookup']
            . '&t='
            . $this->token_params['token'];

        $this->followingRedirects()
            ->call('Get', $url)
            ->assertSee('Inbox');
    }

    /** @test */
    public function invalid_validate_link_does_not_validate_user()
    {
        $this->assertNull($this->user->validated_at);

        $url = '/validate-user?l=thisisfake&t=' . $this->token_params['token'];

        $this->call('Get', $url);

        $this->assertNull($this->user->fresh()->validated_at);
    }
}
