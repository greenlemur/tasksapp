<?php

namespace Tests\Feature;

use App\Action;
use App\Client;
use App\Project;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function opt_in_checkbox_unchecked_leaves_opt_in_unset()
    {
        $this->assertEquals(0, $this->user->agenda_opt_in);

        $this->call("POST", '/admin/agendapref');

        $this->assertEquals(0, $this->user->agenda_opt_in);
    }

    /** @test */
    public function opt_in_checkbox_checked_sets_opt_in()
    {
        $this->assertEquals(0, $this->user->agenda_opt_in);

        $this->call("POST", '/admin/agendapref', [
            'agenda_opt_in' => 1
        ]);

        $this->assertEquals(1, $this->user->agenda_opt_in);
    }

    /** @test */
    public function stats_table_shows_correct_figures()
    {
        $aMonthAgo = Carbon::now()->subDays(30);

        $clients = Client::factory(3)->create();
        $projects = Project::factory(5)->create([ 'client_id' => $clients[0]->id ]);
        $actions = Action::factory(10)->create([
            'client_id' => $clients[0]->id,
            'project_id' => $projects[2]->id
        ]);

        $clients[0]->update([ 'created_at' => $aMonthAgo ]);
        $projects[2]->update([ 'created_at' => $aMonthAgo ]);
        $projects[3]->update([ 'created_at' => $aMonthAgo ]);
        foreach ([1, 2, 3] as $id) {
            $actions[$id]->update([ 'created_at' => $aMonthAgo ]);
        }
        Client::find($clients[1]->id)->delete();
        Project::find($projects[0]->id)->delete();
        Action::find($actions[0]->id)->delete();

        $response = $this->call("GET", '/admin');

        $response->assertSeeInOrder([
            'Last 7 days',
            '<td>Actions added</td><td class="numbers">7</td>',
            '<td>Actions completed</td><td class="numbers">1</td>',
            '<td>Projects added</td><td class="numbers">3</td>',
            '<td>Projects completed</td><td class="numbers">1</td>',
            '<td>Clients added</td><td class="numbers">2</td>',
            '<td>Clients removed</td><td class="numbers">1</td>',
            'All time',
            '<td>Actions added</td><td class="numbers">10</td>',
            '<td>Actions completed</td><td class="numbers">1</td>',
            '<td>Projects added</td><td class="numbers">5</td>',
            '<td>Projects completed</td><td class="numbers">1</td>',
            '<td>Clients added</td><td class="numbers">3</td>',
            '<td>Clients removed</td><td class="numbers">1</td>',
        ], false);
    }

    /** @test */
    public function client_order_changed_on_form_submission()
    {
        // ASSEMBLE
        $client1 = Client::factory()->create();
        $client2 = Client::factory()->create();
        $client3 = Client::factory()->create();

        // ACT
        $this->assertEquals(1, $client1->order);
        $this->assertEquals(2, $client2->order);
        $this->assertEquals(3, $client3->order);

        $this->call('POST', '/admin/saveclientorder', [
            'client_' . $client1->id => 3,
            'client_' . $client2->id => 2,
            'client_' . $client3->id => 1,
        ]);

        // ASSERT
        $this->assertEquals(3, $client1->fresh()->order);
        $this->assertEquals(2, $client2->fresh()->order);
        $this->assertEquals(1, $client3->fresh()->order);
    }
}
