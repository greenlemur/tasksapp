<?php

namespace Tests\Feature\Note;

use App\Note;
use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FormTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function create_note_form_shows_correctly()
    {
        $ajax_response = $this->json('GET', '/note/create');
        $static_response = $this->call('GET', '/note/create');

        $ajax_response->assertSee('<h3>Create a note</h3>', false);
        $static_response->assertSee('<h3>Create a note</h3>', false);
    }
    /** @test */
    public function update_note_form_shows_correctly()
    {
        $note = Note::factory()->create();

        $ajax_response = $this->json('GET', '/note/' . $note->id . '/edit');
        $static_response = $this->call('GET', '/note/' . $note->id . '/edit');

        $ajax_response->assertSee('<h3>Update note</h3>', false);
        $ajax_response->assertSee('name="title" type="text" value="Fake note"', false);
        $static_response->assertSee('<h3>Update note</h3>', false);
        $static_response->assertSee('name="title" type="text" value="Fake note"', false);
    }
}
