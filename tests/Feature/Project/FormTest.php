<?php

namespace Tests\Feature\Project;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FormTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function create_project_form_shows_correctly()
    {
        $this->be($this->user);
        $ajax_response = $this->json('GET', '/project/create');
        $static_response = $this->call('GET', '/project/create');

        $ajax_response->assertSee('<h3>Create new project</h3>', false);
        $static_response->assertSee('<h3>Create new project</h3>', false);
    }
    /** @test */
    public function create_project_form_shows_with_selected_client()
    {
        $this->be($this->user);
        $client = Client::factory()->create();
        $ajax_response = $this->json('GET', '/project/create/' . $client->id);
        $static_response = $this->call('GET', '/project/create/' . $client->id);

        $ajax_response
            ->assertSee('<h3>Create new project</h3>', false)
            ->assertSee('<option value="' . $client->id . '" selected', false);

        $static_response
            ->assertSee('<h3>Create new project</h3>', false)
            ->assertSee('<option value="' . $client->id . '" selected', false);
    }
    /** @test */
    public function create_project_form_shows_with_client_set_from_session()
    {
        $this->be($this->user);
        $client = Client::factory()->create();

        session([ 'client_id' => $client->id ]);

        $ajax_response = $this->json('GET', '/project/create');
        $static_response = $this->call('GET', '/project/create');

        $ajax_response
            ->assertSee('<h3>Create new project</h3>', false)
            ->assertSee('<option value="' . $client->id . '" selected', false);

        $static_response
            ->assertSee('<h3>Create new project</h3>', false)
            ->assertSee('<option value="' . $client->id . '" selected', false);
    }
    /** @test */
    public function update_project_form_shows_correctly()
    {
        // Arrange
        $this->be($this->user);
        $client = Client::factory()->create();
        $project = Project::factory()->create([
            'title' => 'Testing the project update form',
            'client_id' => $client->id
        ]);

        // Act
        $ajax_response = $this->json('GET', '/project/' . $project->id . '/edit');
        $static_response = $this->call('GET', '/project/' . $project->id . '/edit');

        // Assert
        $ajax_response
            ->assertSee('<h3>Update project</h3>', false)
            ->assertSee('Testing the project update form', false)
            ->assertSee('<option value="' . $client->id . '" selected >', false);
        $static_response
            ->assertSee('<h3>Update project</h3>', false)
            ->assertSee('Testing the project update form', false)
            ->assertSee('<option value="' . $client->id . '" selected >', false);
    }
    /** @test */
    public function static_form_shown_again_if_create_another_is_checked()
    {
        // Arrange
        $this->be($this->user);
        $client = Client::factory()->create();

        $response = $this->call('POST', '/project', [
            'title' => 'Test test test',
            'notes' => 'Lots of notes',
            'client_id' => $client->id,
            'addAnotherProject' => 'on'
        ]);
        $project = Project::orderBy('id', 'desc')->first();

        $this->assertEquals(1, $project->client_order);
        $this->assertEquals($this->user->id, $project->user_id);
        $response->assertRedirect('/project/create');
    }
}
