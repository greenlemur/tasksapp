<?php

namespace Tests\Feature\Project;

use App\User;
use App\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function correct_data_returned_for_showing_project()
    {
        // Arrange
        $this->be($this->user);
        $project = Project::factory()->create([ 'title' => 'Testing the show method' ]);

        $response = $this->call('GET', '/project/' . $project->id . '/show');

        $response->assertSeeInOrder([
            '<div class="project-row">',
            '<span class="project-title is-open">',
            'Testing the show method',
        ], false);
    }
}
