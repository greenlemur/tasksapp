<?php

namespace Tests\Feature\Project;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function project_with_live_actions_will_not_delete()
    {
        // Arrange
        $this->be($this->user);
        $client = Client::factory()->create();
        $project = Project::factory()->create(['client_id' => $client->id]);
        Action::factory(3)->create(['client_id' => $client->id, 'project_id' => $project->id]);

        // Act
        $response = $this->json('DELETE', '/project/' . $project->id . '/delete');

        // Assert
        $response->assertStatus(400);
    }
    /** @test */
    public function can_delete_project_once_actions_are_completed()
    {
        // Arrange
        $this->be($this->user);
        Client::factory()->create();
        $project = Project::factory()->create(['client_id' => 1]);
        $actions = Action::factory(3)->create(['client_id' => 1, 'project_id' => 1]);

        // Act
        foreach ($actions as $action) {
            $action->deleted_at = Carbon::now();
            $action->save();
        }
        $response = $this->json('DELETE', '/project/' . $project->id . '/delete');

        // Assert
        $response->assertStatus(200);
        $this->assertNotNull(Project::withTrashed()->find($project->id)->deleted_at);
    }
    /** @test */
    public function other_projects_reorder_when_project_is_deleted()
    {
        // Arrange
        $this->be($this->user);
        $client = Client::factory()->create();
        $projects = Project::factory(4)->create(['client_id' => $client->id]);

        // Act
        $response = $this->json('DELETE', '/project/' . $projects[1]->id . '/delete');

        // Assert
        $this->assertEquals(1, $projects[0]->client_order);
        $this->assertEquals(0, Project::withTrashed()->find($projects[1]->id)->client_order);
        $this->assertEquals(2, $projects[2]->fresh()->client_order);
        $this->assertEquals(3, $projects[3]->fresh()->client_order);
    }
}
